## Zadanie 1 - Sortowanie bąbelkowe i zoptymalizowane

```
Dlugosc tablicy:  10000 
Czas sortowania normalnym bubblesortem: 0.49 s (490 ms)
Czas sortowania zoptimalizowanym bubblesortem: 0.50 s (500 ms)
Czas sortowania na posortowanej tablicy - normalny bubblesort: 0.26 s (260 ms)
Czas sortowania na posortowanej tablicy - zoptymalizowany bubblesort: 0.00 s (0 ms)
Czas sortowania na posortowanej odwrotnie tablicy - normalny bubblesort: 0.42 s (420 ms)
Czas sortowania na posortowanej odwrotnie tablicy - zoptymalizowany bubblesort: 0.42 s (420 ms)
```

```
Dlugosc tablicy: 100000
Czas sortowania normalnym bubblesortem: 56.73 s (56730 ms)
Czas sortowania zoptimalizowanym bubblesortem: 56.34 s (56340 ms)
Czas sortowania na posortowanej tablicy - normalny bubblesort: 26.21 s (26210 ms)
Czas sortowania na posortowanej tablicy - zoptymalizowany bubblesort: 0.00 s (0 ms)
Czas sortowania na posortowanej odwrotnie tablicy - normalny bubblesort: 42.05 s (42050 ms)
Czas sortowania na posortowanej odwrotnie tablicy - zoptymalizowany bubblesort: 43.04 s (43040 ms)
```

### Odpowiedz
W zależności od zadania. Gdy robimy operacje na tablicy które są trywialne wplywa korzystniee zastosowanie zoptymalizowanegp sortowania bąbelkowe. W przeciwnym przypadku sortowanie bąbelkowe jest lepszą opcją.

## Zadanie 2 i 3 - {Hoare,Lomuto}-Partition
```
  Plik: dane.txt
#################
 Hoare: 31
Lomuto: 46
```
```
  Plik: dane1.txt
#################
 Hoare: 45
Lomuto: 40
```

```
  Plik: dane2.txt
#################
 Hoare: 23
Lomuto: 44
```

```
  Plik: dane3.txt
#################
 Hoare: 8
Lomuto: 12
```
## Informacje
- Język: C
- Linux lp-dtusk 3.10-3-amd64 #1 SMP Debian 3.10.11-1 (2013-09-10) x86_64 GNU/Linux

