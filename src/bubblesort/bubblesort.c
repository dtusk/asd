#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

#define DLUGOSC 10000

//#define __DEBUG__
#ifdef __DEBUG__
static void printarray(int A[DLUGOSC]);
#endif
static void generate(int A[DLUGOSC], int B[DLUGOSC]);
static void bubblesort(int A[DLUGOSC]);
static void bubblesort_optimize(int A[DLUGOSC]);
static void bubblesort_reverse(int A[DLUGOSC]);


int main() {
  srand(time(NULL));
  int A[DLUGOSC];
  int B[DLUGOSC];
  generate(A,B);
#ifdef __DEBUG__
  printarray(A);
#endif
  printf("Dlugosc tablicy: %d\n", DLUGOSC);
  clock_t start = clock();
  bubblesort(A);
  clock_t end = clock() - start;
#ifdef __DEBUG__
  printarray(A);
#endif
  printf("Czas sortowania normalnym bubblesortem: %.2f s (%.0f ms)\n",
	 ((float)end)/CLOCKS_PER_SEC,
	 (((float)end)/CLOCKS_PER_SEC) * 1000);
#ifdef __DEBUG__
  printarray(B);
#endif
  start = clock();
  bubblesort_optimize(B);
  end = clock() - start;
  printf("Czas sortowania zoptimalizowanym bubblesortem: %.2f s (%.0f ms)\n", 
	 ((float)end)/CLOCKS_PER_SEC, 
	 (((float)end)/CLOCKS_PER_SEC) *1000); 

#ifdef __DEBUG__
  printarray(A);
#endif
  start = clock();
  bubblesort(A);
  end = clock() - start;
  printf("Czas sortowania na posortowanej tablicy - normalny bubblesort: %.2f s (%.0f ms)\n",

	 ((float)end)/CLOCKS_PER_SEC,
	 (((float)end)/CLOCKS_PER_SEC) * 1000);
  start = clock();
  bubblesort_optimize(B);
  end = clock() - start;
  printf("Czas sortowania na posortowanej tablicy - zoptymalizowany bubblesort: %.2f s (%.0f ms)\n", 
	 ((float)end)/CLOCKS_PER_SEC,
	 (((float)end)/CLOCKS_PER_SEC) * 1000);


  bubblesort_reverse(A);
  bubblesort_reverse(B);

#ifdef __DEBUG__
  printarray(A);
#endif
  start = clock();
  bubblesort(A);
  end = clock() - start;
  printf("Czas sortowania na posortowanej odwrotnie tablicy - normalny bubblesort: %.2f s (%.0f ms)\n",

	 ((float)end)/CLOCKS_PER_SEC,
	 (((float)end)/CLOCKS_PER_SEC) * 1000);
  start = clock();
  bubblesort_optimize(B);
  end = clock() - start;
  printf("Czas sortowania na posortowanej odwrotnie tablicy - zoptymalizowany bubblesort: %.2f s (%.0f ms)\n", 
	 ((float)end)/CLOCKS_PER_SEC,
	 (((float)end)/CLOCKS_PER_SEC) * 1000);

  
#ifdef __DEBUG__
  printarray(B);
#endif
  return 0;
}

#ifdef __DEBUG__
void printarray(int A[DLUGOSC])
{
  for (int i = 0; i < DLUGOSC; i++)
  {
    printf("%d\n", A[i]);
  }
  printf("\n");
}
#endif

void generate(int A[], int B[])
{
  for (int i = 0; i < DLUGOSC; i++)
  {
    int tmp = rand();
    A[i] = tmp;
    B[i] = tmp;
  }
}

void bubblesort(int A[])
{
  int n = DLUGOSC - 1;
  do
  {
     for (int i = 0; i < n; i++)
     {
       if (A[i] > A[i+1])
       {
         int tmp = A[i];
         A[i] = A[i+1];
         A[i+1] = tmp;
       }
     }
    n--;
  } while (n > 1);
}

void bubblesort_optimize(int A[]) {
  int n = DLUGOSC - 1;
  bool suap;
  do
  {
    suap = false;
    for (int j = 0; j < n; j++)
    {
      if (A[j] > A[j + 1])
      {
        int tmp = A[j];
        A[j] = A[j + 1];
        A[j + 1] = tmp;
        suap = true;
      }
    }
    if (!suap)
      break;
    n--;
  } while (n > 1);
}


void bubblesort_reverse(int A[])
{
  int n = DLUGOSC - 1;
  do
  {
     for (int i = 0; i < n; i++)
     {
       if (A[i] < A[i+1])
       {
         int tmp = A[i];
         A[i] = A[i+1];
         A[i+1] = tmp;
       }
     }
    n--;
  } while (n > 1);
}
