#include <stdio.h>

#define DLUGOSC 50 // skad? sed 's/ /\n/g' [plik] | wc -l

//#define __DEBUG__
#ifdef __DEBUG__
static void printArray(int A[]);
#endif
static void loadDataFromFile(const char* filename, int A[]);
static int hoarePartition(int A[]);
static int lomutoPartition(int A[]);

int main(int argc, char **argv) {
  if (argc == 2) {
    int A[DLUGOSC];
    loadDataFromFile(argv[1], A);
    int B[DLUGOSC];
    memcpy(B,A,sizeof(A));
#ifdef __DEBUG__
    printArray(B);
#endif
    printf("  Plik: %s\n#################\n",argv[1]);
    printf(" Hoare: %d\n", hoarePartition(A));
    printf("Lomuto: %d\n", lomutoPartition(B));
  } else {
    printf("Usage: [program] filename\n");
  }
  return 0;
}

#ifdef __DEBUG__
void printArray(int A[]) {
  for (int i = 0; i < DLUGOSC; i++) {
    printf("%d ", A[i]);
  }
  printf("\n");
}
#endif

void loadDataFromFile(const char* filename, int A[]) {
  FILE *p = fopen(filename, "r");
  int i;
  int j = 0;
  fscanf (p, "%d", &i);
  while (!feof(p)) {
    A[j++] = i;
    fscanf (p, "%d", &i);
  }
  fclose(p);
}

int hoarePartition(int A[]) {
  int x = A[0];
  int i = -1;
  int j = DLUGOSC + 1;
  for (;;) {
    do {
      j--;
    } while (A[j] < x);

    do {
      i++;
    } while (A[i] > x);

    if (i < j) {
      int tmp = A[i];
      A[i] = A[j];
      A[j] = tmp;
    } else {
      break;
    }
  }

  return j;
}

int lomutoPartition(int A[]) {
  int x = A[DLUGOSC-1];
  int i = -1;
  for (int j = 0; j < DLUGOSC - 1; j++) {
    if (A[j] <= x) {
      i++;
      int tmp = A[i];
      A[i] = A[j];
      A[j] = tmp;
    }
  }
  int tmp = A[DLUGOSC-1];
  A[DLUGOSC-1] = A[i+1];
  A[i+1] = tmp;
  return i + 1;
}
