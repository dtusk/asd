#include <stdio.h>
#include <stdlib.h>

void maxHeap( int *a, int start, int count);

#define SWAP(r,s)  {int t=r; r=s; s=t; }

/*
 * "Kontroler" funkcji sortującej
 */
void heapsort( int *a, int count) {
    int start, end;

    /* heapify */ // tworzenie kopca
    for (start = (count-2)/2; start >=0; start--) {
        maxHeap( a, start, count);
    }

    // sortowanie
    for (end=count-1; end > 0; end--) {
        SWAP(a[end],a[0]);
        maxHeap(a, 0, end);
    }
}
/*
 * funkcja jest odpowiedzialna za mechanizm
 * sortowania przez wartosc maksymalną
 */
void maxHeap( int *a, int start, int end) {
    int root = start;

    while ( root*2+1 < end ) {
        int child = 2*root + 1;
        // child - lewy syn
        // child+1 - prawy syn
        if ((child + 1 < end) && a[child] < a[child+1]) {
            child += 1;
        }
        if (a[root] < a[child]) {
            SWAP( a[child], a[root] );
            root = child;
        }
        else
            return;
    }
}


int main() {
    int valsToSort[] = {
        1, 50, 5, -1, 301, 0, 40,
        -18, 88, 30, -37, 3012, 49, 10};
#define LEN (sizeof(valsToSort)/sizeof(valsToSort[0]))
    heapsort(valsToSort, LEN);
    for (int i=0; i<LEN; i++) printf(" %d ", valsToSort[i]);
    printf("\n");
    return 0;
}
